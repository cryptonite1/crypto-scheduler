const fetch = require("cross-fetch");
const Cryptocurrency = require("../models/Cryptocurrency");
const mongoose = require("mongoose");

//Creer ma liste de cryptomonnaie et l inserer dans ma base de donnéé
async function myfunction() {
    const response = await fetch("https://example-data.draftbit.com/crypto?_limit=50");
    const data = await response.json(); //extract JSON from the http response
    for (i=0; i<data.length; i++){
      const current_datetime = new Date()
      var customDate = current_datetime.getFullYear() + "-" + (current_datetime.getMonth() + 1) + "-" + current_datetime.getDate() + " " + current_datetime.getHours() + ":" + current_datetime.getMinutes() + ":" + current_datetime.getSeconds();
      
      var params = new Cryptocurrency({
        name: data[i].name,
        symbol: data[i].symbol,
        price: data[i].price,
        date : customDate
      });
      
      var error_result = false;
      await params.save((err) => {
        if (err) {
          throw err;
        }
        
      });
      
    };
    console.log("Cryptocurencies created with success");
  };

  //fonction permettant d'eviter la mise en veille d'Heroku
var http = require("http");

function startKeepAlive() {

    var options = {
      host: 'calm-dusk-53201.herokuapp.com',
      port: 80,
      path: '/'
    };
    http.get(options, function(res) {
      res.on('data', function(chunk) {
        try {
          //optional logging... disable after it's working
          console.log("HEROKU RESPONSE: " + chunk);
        } catch(err) {
          console.log(err.message);
        } 
      });
    }).on('error', function(err) {
      console.log("Error: " + err.message);
    });
  }

exports.startKeepAlive = startKeepAlive;
exports.myfunction = myfunction;