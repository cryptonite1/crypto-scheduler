const mongoose = require("mongoose")
const Schema = mongoose.Schema;

//Modèle base de données
const CryptoSchema = new Schema({
    name: String,
    symbol: String,
    price: Number,
    date : String
});

module.exports = mongoose.model("Cryptocurencies", CryptoSchema)